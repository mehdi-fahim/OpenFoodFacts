export class Annonce {

    titre : string;
    description: string;
    id_hebergeur: number;
    email: string;
    prix: number;
    image: {
        
    };
    adresse: string;
    type_habitat: string;
    nb_toilette: number;
    nb_salle_de_bain: number;
    nb_chambre: number;
    nbpersonne: number;
    surface: number;
    published: boolean;
    avis: {
        titre: string;
        commentaire: string;
        note: number;
    }
    reservation: {
        
    }
    entreprise: {
        name: string;
        siret: string;
        adresse: string;
        statut_juridique: string;
    }
    services: {
        name: string;
        categorie: {
            id: number;
            name: string;
        }
    }
}