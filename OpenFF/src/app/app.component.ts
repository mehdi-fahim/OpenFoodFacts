import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Annonce } from './annonce';
import { AnnonceService } from './annonce.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AnnonceResult {

  constructor(private annonceService: AnnonceService) {}

  @Output() close = new EventEmitter();

  annonce:Annonce = new Annonce();

  recupAnnonce(event: any): void {
  this.annonceService
    .recupAnnonce(event.target.id)
    .then(
      response => {
        this.annonce = response;
      })  
    }
}
