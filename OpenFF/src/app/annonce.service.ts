import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Annonce } from './annonce';

@Injectable()
export class AnnonceService {

    private apiUrl = 'http://127.0.0.1:8000/api/v1/annonce/';
    public annonce = new Annonce();

    constructor(private http: Http) {

    }

    recupAnnonce(idAnnonce: any) {
        return this.http
        .get(this.apiUrl+idAnnonce)
        .toPromise()
        .then(res => <Annonce> res.json());
    }
    
    
}