import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { AnnonceResult } from './app.component';
import { AnnonceService } from './annonce.service';
import { HttpModule } from '@angular/http';


@NgModule({
  declarations: [
    AnnonceResult
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    AnnonceService
  ],
  bootstrap: [
    AnnonceResult
  ]
})
export class AppModule { }
